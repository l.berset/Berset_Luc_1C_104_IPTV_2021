Module 104 Luc Berset 1C

Faire fonctionner le site :

BUT : Le site est accessible en local ou via le net si vous avez ouvert vos ports

- Installer Python
- Installer et démarrer le serveur MySql (uwamp ou xamp ou mamp, etc)
- Installer un éditeur ( PyCharm, Visual Studio Code,IntelliJ, etc).

- Ouvrir le fichier ".env" à la racine du projet, contrôler les indications* de connexion pour la bd.(*Les ajuster si nécessaire)
- Choisir un interpreter python sur votre éditeur

 #Pycharm : cliquer sur select interpreter




Puis dans le répertoire racine du projet :

- Lancer le fichier "flask_server.py", il permet juste d'executer le serveur flask.




Les prérequis devraient s'installer automatiquement et de même pour l'importation de la base de donné 

En cas d'erreur:

- Vérifier que le nom et le chemin de la DB est correct dans le fichier ".env"
- Vérifier que la base de donnée se trouve bien dans "APP/database
- Executer le fichier "ImportDumpSql.py"




L'IP du serveur et le Port s'affiche dans le terminal, cliquer dessus ou copier la dans un navigateur

En cas de problème :

vérifier que l'IP et le Port sont bien dans le fichier ".env"
127.0.0.1 est l'Ip Localhost






Comment fonctionne le site :

Base du site
Le site permet la gestion d'une base de données ayant pour but de gérer les informations d'une personne qui aurait pu louer un appareil.
Il est possible d'ajouter/lier plusieurs éléments à une personne comme :

- Ajouter des informations personnelles :
	- Ajouter une adresse
	- Ajouter un mail
	- Ajouter un téléphone

- Lier un appareil de location à son "propriétaire actuelle"



Ces données seront utiles afin de déterminer la détenance d'un appareil en location et de définir plus exactement l'identité du préstataire "du service".




