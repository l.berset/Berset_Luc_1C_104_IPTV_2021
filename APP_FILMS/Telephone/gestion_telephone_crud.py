"""
    Fichier : gestion_telephones_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les telephones.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.Telephone.gestion_telephone_wtf_forms import FormWTFAjouterTelephone
from APP_FILMS.Telephone.gestion_telephone_wtf_forms import FormWTFDeleteTelephone
from APP_FILMS.Telephone.gestion_telephone_wtf_forms import FormWTFUpdateTelephone

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /telephones_afficher
    
    Test : ex : http://127.0.0.1:5005/telephones_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_telephone_sel = 0 >> tous les telephones.
                id_telephone_sel = "n" affiche le telephone dont l'id est "n"
"""


@obj_mon_application.route("/telephone_afficher/<string:order_by>/<int:Id_Personne_sel>", methods=['GET', 'POST'])
def telephone_afficher(order_by, Id_Personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion telephone ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionTelephones {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and Id_Personne_sel == 0:
                    strsql_telephone_afficher = """SELECT Id_Telephone, Num_Tel FROM t_telephone ORDER BY Id_Telephone ASC"""
                    mc_afficher.execute(strsql_telephone_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_telephone"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du telephone sélectionné avec un nom de variable
                    valeur_id_telephone_selected_dictionnaire = {"value_id_telephone_selected": Id_Personne_sel}
                    strsql_telephone_afficher = """SELECT Id_Telephone, Num_Tel FROM t_telephone  WHERE Id_Telephone = %(value_id_telephone_selected)s"""

                    mc_afficher.execute(strsql_telephone_afficher, valeur_id_telephone_selected_dictionnaire)
                else:
                    strsql_telephone_afficher = """SELECT Id_Telephone, Num_Tel FROM t_telephone ORDER BY Id_Telephone DESC"""

                    mc_afficher.execute(strsql_telephone_afficher)

                data_personne = mc_afficher.fetchall()

                print("data_personne ", data_personne, " Type : ", type(data_personne))

                # Différencier les messages si la table est vide.
                if not data_personne and Id_Personne_sel == 0:
                    flash("""La table "t_telephone" est vide. !!""", "warning")
                elif not data_personne and Id_Personne_sel > 0:
                    # Si l'utilisateur change l'id_telephone dans l'URL et que le telephone n'existe pas,
                    flash(f"La personne_sel demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_telephone" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données telephones affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. telephone_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} telephone_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("telephone/telephone_afficher.html", data=data_personne)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /telephones_ajouter
    
    Test : ex : http://127.0.0.1:5005/telephones_ajouter
    
    Paramètres : sans
    
    But : Ajouter un telephone pour un film
    
    Remarque :  Dans le champ "name_telephone_html" du formulaire "telephones/telephones_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/telephone_ajouter", methods=['GET', 'POST'])
def telephone_ajouter_wtf():
    form = FormWTFAjouterTelephone()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion telephones ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestiontelephones {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                num_telephone_wtf = form.num_telephone_wtf.data
                num_telephone = num_telephone_wtf.lower()


                valeurs_insertion_dictionnaire = {"value_num": num_telephone}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_Nom_Personne = """INSERT INTO t_telephone (Id_Telephone,Num_Tel) VALUES (NULL,%(value_num)s"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_Nom_Personne, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('telephone_afficher', order_by='DESC', Id_Personne_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_personne_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_personne_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion telephone CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("telephone/telephone_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /telephone_update
    
    Test : ex cliquer sur le menu "telephones" puis cliquer sur le bouton "EDIT" d'un "telephone"
    
    Paramètres : sans
    
    But : Editer(update) un telephone qui a été sélectionné dans le formulaire "telephones_afficher.html"
    
    Remarque :  Dans le champ "nom_telephone_update_wtf" du formulaire "telephones/telephone_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/telephone_update", methods=['GET', 'POST'])
def telephone_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_telephone"
    id_telephone_update = request.values['id_telephone_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateTelephone()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "telephone_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            num_telephone_wtf = form_update.num_telephone_update_wtf.data
            num_telephone = num_telephone_wtf.lower()


            valeur_update_dictionnaire = {"value_id_telephone": id_telephone_update, "value_num": num_telephone}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intituletelephone = """UPDATE t_telephone SET Num_Tel= %(value_num)s WHERE Id_Telephone = %(value_id_telephone)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituletelephone, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_telephone_update"
            return redirect(url_for('telephone_afficher', order_by="ASC", Id_Personne_sel=id_telephone_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_telephone" et "intitule_telephone" de la "t_telephone"
            str_sql_id_telephone = "SELECT Id_Telephone, Num_Tel FROM t_telephone WHERE Id_Telephone = %(value_id_telephone)s"
            valeur_select_dictionnaire = {"value_id_telephone": id_telephone_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_telephone, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom telephone" pour l'UPDATE
            data_nom_telephone = mybd_curseur.fetchone()
            print("data_nom_telephone ", data_nom_telephone, " type ", type(data_nom_telephone), " telephone ",
                  data_nom_telephone["Num_Tel"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "telephone_update_wtf.html"
            form_update.num_telephone_update_wtf.data = data_nom_telephone["Num_Tel"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans telephone_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans telephone_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans telephone_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans telephone_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("telephone/telephone_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /telephone_delete
    
    Test : ex. cliquer sur le menu "telephones" puis cliquer sur le bouton "DELETE" d'un "telephone"
    
    Paramètres : sans
    
    But : Effacer(delete) un telephone qui a été sélectionné dans le formulaire "telephones_afficher.html"
    
    Remarque :  Dans le champ "nom_telephone_delete_wtf" du formulaire "telephones/telephone_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/telephone_delete", methods=['GET', 'POST'])
def telephone_delete_wtf():
    data_films_attribue_telephone_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_telephone"
    id_telephone_delete = request.values['id_telephone_btn_delete_html']

    # Objet formulaire pour effacer le telephone sélectionné.
    form_delete = FormWTFDeleteTelephone()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("telephone_afficher", order_by="ASC", Id_Personne_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "telephones/telephone_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_telephone_delete = session['data_films_attribue_telephone_delete']
                print("data_films_attribue_telephone_delete ", data_films_attribue_telephone_delete)

                flash(f"Effacer le telephone de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer telephone" qui va irrémédiablement EFFACER le telephone
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_telephone": id_telephone_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_telephone = """DELETE FROM t_telephone WHERE Id_Telephone = %(value_id_telephone)s"""
                str_sql_delete_idtelephone = """DELETE FROM t_telephone WHERE Id_Telephone = %(value_id_telephone)s"""
                # Manière brutale d'effacer d'abord la "fk_telephone", même si elle n'existe pas dans la "t_telephone_film"
                # Ensuite on peut effacer le telephone vu qu'il n'est plus "lié" (INNODB) dans la "t_telephone_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_telephone, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idtelephone, valeur_delete_dictionnaire)

                flash(f"telephone définitivement effacé !!", "success")
                print(f"telephone définitivement effacé !!")

                # afficher les données
                return redirect(url_for('telephone_afficher', order_by="ASC", Id_Personne_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_telephone": id_telephone_delete}
            print(id_telephone_delete, type(id_telephone_delete))

            # Requête qui affiche tous les films qui ont le telephone que l'utilisateur veut effacer
            str_sql_telephones_films_delete = """SELECT Id_Telephone, Num_Tel FROM t_telephone WHERE Id_Telephone= %(value_id_telephone)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_telephones_films_delete, valeur_select_dictionnaire)
            data_films_attribue_telephone_delete = mybd_curseur.fetchall()
            print("data_films_attribue_telephone_delete...", data_films_attribue_telephone_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "telephones/telephone_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_telephone_delete'] = data_films_attribue_telephone_delete

            # Opération sur la BD pour récupérer "id_telephone" et "intitule_telephone" de la "t_telephone"
            str_sql_id_telephone = "SELECT Id_Telephone, Num_Tel FROM t_telephone WHERE Id_Telephone = %(value_id_telephone)s"

            mybd_curseur.execute(str_sql_id_telephone, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom telephone" pour l'action DELETE
            data_nom_telephone = mybd_curseur.fetchone()
            print("data_nom_telephone ", data_nom_telephone, " type ", type(data_nom_telephone), " Num_Tel ",
                  data_nom_telephone["Num_Tel"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "telephone_delete_wtf.html"
            form_delete.num_telephone_delete_wtf.data = data_nom_telephone["Num_Tel"]


            # Le bouton pour l'action "DELETE" dans le form. "telephone_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans telephone_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans telephone_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans telephone_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans telephone_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("telephone/telephone_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_telephone_delete)
