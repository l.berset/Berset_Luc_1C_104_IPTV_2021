"""
    Fichier : gestion_telephones_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterTelephone(FlaskForm):
    """
        Dans le formulaire "telephones_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    n_telephone_regexp = "[0-9_.-]"

    num_telephone_wtf = StringField("Clavioter le Num_Tel ", validators=[Length(min=1, max=5, message="min 1000 max 9999"),
                                                                   Regexp(n_telephone_regexp,
                                                                          message="Pas de ")
                                                                   ])
    submit = SubmitField("Enregistrer le telephone")


class FormWTFUpdateTelephone(FlaskForm):
    """
        Dans le formulaire "telephone_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """


    n_telephone_regexp = "[0-9_.-]"

    num_telephone_update_wtf = StringField("Clavioter le Num_Tel ", validators=[Length(min=1, max=5, message="min 1000 max 9999"),
                                                                   Regexp(n_telephone_regexp,
                                                                          message="Pas de ")
                                                                   ])

    submit = SubmitField("Modifier le telephone")


class FormWTFDeleteTelephone(FlaskForm):
    """
        Dans le formulaire "telephone_delete_wtf.html"

        nom_telephone_delete_wtf : Champ qui reçoit la valeur du telephone, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "telephone".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_telephone".
    """
    num_telephone_delete_wtf = StringField("Num_Tel:")
    submit_btn_del = SubmitField("Effacer cette personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
