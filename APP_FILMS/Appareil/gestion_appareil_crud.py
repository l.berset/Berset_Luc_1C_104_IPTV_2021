"""
    Fichier : gestion_appareils_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les appareils.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.Appareil.gestion_appareil_wtf_forms import FormWTFAjouterAppareil
from APP_FILMS.Appareil.gestion_appareil_wtf_forms import FormWTFDeleteAppareil
from APP_FILMS.Appareil.gestion_appareil_wtf_forms import FormWTFUpdateAppareil

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /appareils_afficher
    
    Test : ex : http://127.0.0.1:5005/appareils_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_appareil_sel = 0 >> tous les appareils.
                id_appareil_sel = "n" affiche le appareil dont l'id est "n"
"""


@obj_mon_application.route("/appareil_afficher/<string:order_by>/<int:Id_Personne_sel>", methods=['GET', 'POST'])
def appareil_afficher(order_by, Id_Personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion appareil ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionAppareils {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and Id_Personne_sel == 0:
                    strsql_appareil_afficher = """SELECT Id_Appareil, NPA, Ville, Rue, N FROM t_appareil ORDER BY Id_Appareil ASC"""
                    mc_afficher.execute(strsql_appareil_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_appareil"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du appareil sélectionné avec un nom de variable
                    valeur_id_appareil_selected_dictionnaire = {"value_id_appareil_selected": Id_Personne_sel}
                    strsql_appareil_afficher = """SELECT Id_Appareil, NPA, Ville, Rue, N FROM t_appareil  WHERE Id_Appareil = %(value_id_appareil_selected)s"""

                    mc_afficher.execute(strsql_appareil_afficher, valeur_id_appareil_selected_dictionnaire)
                else:
                    strsql_appareil_afficher = """SELECT Id_Appareil, NPA, Ville, Rue, N FROM t_appareil ORDER BY Id_Appareil DESC"""

                    mc_afficher.execute(strsql_appareil_afficher)

                data_personne = mc_afficher.fetchall()

                print("data_personne ", data_personne, " Type : ", type(data_personne))

                # Différencier les messages si la table est vide.
                if not data_personne and Id_Personne_sel == 0:
                    flash("""La table "t_appareil" est vide. !!""", "warning")
                elif not data_personne and Id_Personne_sel > 0:
                    # Si l'utilisateur change l'id_appareil dans l'URL et que le appareil n'existe pas,
                    flash(f"La personne_sel demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_appareil" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données appareils affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. appareil_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} appareil_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("appareil/appareil_afficher.html", data=data_personne)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /appareils_ajouter
    
    Test : ex : http://127.0.0.1:5005/appareils_ajouter
    
    Paramètres : sans
    
    But : Ajouter un appareil pour un film
    
    Remarque :  Dans le champ "name_appareil_html" du formulaire "appareils/appareils_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/appareil_ajouter", methods=['GET', 'POST'])
def appareil_ajouter_wtf():
    form = FormWTFAjouterAppareil()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion appareils ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionappareils {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                npa_appareil_wtf = form.npa_appareil_wtf.data
                npa_appareil = npa_appareil_wtf.lower()
                ville_wtf = form.ville_appareil_wtf.data
                ville = ville_wtf.capitalize()
                rue_wtf = form.rue_appareil_wtf.data
                rue = rue_wtf.capitalize()
                n_wtf = form.n_appareil_wtf.data
                n = n_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_npa": npa_appareil,"value_ville":ville,"value_rue":rue,"value_n":n}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_Nom_Personne = """INSERT INTO t_appareil (Id_Appareil,NPA,Ville,Rue,N) VALUES (NULL,%(value_npa)s,%(value_ville)s,%(value_rue)s,%(value_n)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_Nom_Personne, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('appareil_afficher', order_by='DESC', Id_Personne_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_personne_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_personne_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion appareil CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("appareil/appareil_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /appareil_update
    
    Test : ex cliquer sur le menu "appareils" puis cliquer sur le bouton "EDIT" d'un "appareil"
    
    Paramètres : sans
    
    But : Editer(update) un appareil qui a été sélectionné dans le formulaire "appareils_afficher.html"
    
    Remarque :  Dans le champ "nom_appareil_update_wtf" du formulaire "appareils/appareil_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/appareil_update", methods=['GET', 'POST'])
def appareil_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_appareil"
    id_appareil_update = request.values['id_appareil_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateAppareil()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "appareil_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            npa_appareil_wtf = form_update.npa_appareil_update_wtf.data
            npa_appareil = npa_appareil_wtf.lower()
            ville_wtf = form_update.ville_appareil_update_wtf.data
            ville = ville_wtf.capitalize()
            rue_wtf = form_update.rue_appareil_update_wtf.data
            rue = rue_wtf.capitalize()
            n_wtf = form_update.n_appareil_update_wtf.data
            n = n_wtf.lower()

            valeur_update_dictionnaire = {"value_id_appareil": id_appareil_update, "value_npa": npa_appareil,
                                          "value_ville": ville, "value_rue": rue, "value_n": n}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intituleappareil = """UPDATE t_appareil SET NPA= %(value_npa)s,
                                                Ville= %(value_ville)s, Rue= %(value_rue)s, N= %(value_n)s WHERE Id_Appareil = %(value_id_appareil)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituleappareil, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_appareil_update"
            return redirect(url_for('appareil_afficher', order_by="ASC", Id_Personne_sel=id_appareil_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_appareil" et "intitule_appareil" de la "t_appareil"
            str_sql_id_appareil = "SELECT Id_Appareil, NPA, Ville, Rue, N FROM t_appareil WHERE Id_Appareil = %(value_id_appareil)s"
            valeur_select_dictionnaire = {"value_id_appareil": id_appareil_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_appareil, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom appareil" pour l'UPDATE
            data_nom_appareil = mybd_curseur.fetchone()
            print("data_nom_appareil ", data_nom_appareil, " type ", type(data_nom_appareil), " appareil ",
                  data_nom_appareil["NPA"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "appareil_update_wtf.html"
            form_update.npa_appareil_update_wtf.data = data_nom_appareil["NPA"]
            form_update.ville_appareil_update_wtf.data = data_nom_appareil["Ville"]
            form_update.rue_appareil_update_wtf.data = data_nom_appareil["Rue"]
            form_update.n_appareil_update_wtf.data = data_nom_appareil["N"]
    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans appareil_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans appareil_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans appareil_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans appareil_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("appareil/appareil_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /appareil_delete
    
    Test : ex. cliquer sur le menu "appareils" puis cliquer sur le bouton "DELETE" d'un "appareil"
    
    Paramètres : sans
    
    But : Effacer(delete) un appareil qui a été sélectionné dans le formulaire "appareils_afficher.html"
    
    Remarque :  Dans le champ "nom_appareil_delete_wtf" du formulaire "appareils/appareil_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/appareil_delete", methods=['GET', 'POST'])
def appareil_delete_wtf():
    data_films_attribue_appareil_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_appareil"
    id_appareil_delete = request.values['id_appareil_btn_delete_html']

    # Objet formulaire pour effacer le appareil sélectionné.
    form_delete = FormWTFDeleteAppareil()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("appareil_afficher", order_by="ASC", Id_Personne_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "appareils/appareil_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_appareil_delete = session['data_films_attribue_appareil_delete']
                print("data_films_attribue_appareil_delete ", data_films_attribue_appareil_delete)

                flash(f"Effacer le appareil de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer appareil" qui va irrémédiablement EFFACER le appareil
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_appareil": id_appareil_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_appareil = """DELETE FROM t_appareil WHERE Id_Appareil = %(value_id_appareil)s"""
                str_sql_delete_idappareil = """DELETE FROM t_appareil WHERE Id_Appareil = %(value_id_appareil)s"""
                # Manière brutale d'effacer d'abord la "fk_appareil", même si elle n'existe pas dans la "t_appareil_film"
                # Ensuite on peut effacer le appareil vu qu'il n'est plus "lié" (INNODB) dans la "t_appareil_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_appareil, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idappareil, valeur_delete_dictionnaire)

                flash(f"appareil définitivement effacé !!", "success")
                print(f"appareil définitivement effacé !!")

                # afficher les données
                return redirect(url_for('appareil_afficher', order_by="ASC", Id_Personne_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_appareil": id_appareil_delete}
            print(id_appareil_delete, type(id_appareil_delete))

            # Requête qui affiche tous les films qui ont le appareil que l'utilisateur veut effacer
            str_sql_appareils_films_delete = """SELECT Id_Appareil, NPA, Ville, Rue, N FROM t_appareil WHERE Id_Appareil= %(value_id_appareil)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_appareils_films_delete, valeur_select_dictionnaire)
            data_films_attribue_appareil_delete = mybd_curseur.fetchall()
            print("data_films_attribue_appareil_delete...", data_films_attribue_appareil_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "appareils/appareil_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_appareil_delete'] = data_films_attribue_appareil_delete

            # Opération sur la BD pour récupérer "id_appareil" et "intitule_appareil" de la "t_appareil"
            str_sql_id_appareil = "SELECT Id_Appareil, NPA, Ville, Rue, N FROM t_appareil WHERE Id_Appareil = %(value_id_appareil)s"

            mybd_curseur.execute(str_sql_id_appareil, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom appareil" pour l'action DELETE
            data_nom_appareil = mybd_curseur.fetchone()
            print("data_nom_appareil ", data_nom_appareil, " type ", type(data_nom_appareil), " NPA ",
                  data_nom_appareil["NPA"],"Ville", data_nom_appareil["Ville"], "Rue", data_nom_appareil["Rue"], "N", data_nom_appareil["N"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "appareil_delete_wtf.html"
            form_delete.npa_appareil_delete_wtf.data = data_nom_appareil["NPA"]
            form_delete.ville_appareil_delete_wtf.data = data_nom_appareil["Ville"]
            form_delete.rue_appareil_delete_wtf.data = data_nom_appareil["Rue"]
            form_delete.n_appareil_delete_wtf.data = data_nom_appareil["N"]

            # Le bouton pour l'action "DELETE" dans le form. "appareil_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans appareil_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans appareil_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans appareil_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans appareil_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("appareil/appareil_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_appareil_delete)
