"""
    Fichier : gestion_appareils_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterAppareil(FlaskForm):
    """
        Dans le formulaire "appareils_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_appareil_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\-]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    rue_appareil_regexp = "^[^0-9()]+$"
    numero_appareil_regexp = "[0-9]"
    n_appareil_regexp = "[0-9_.-]"

    npa_appareil_wtf = StringField("Clavioter le NPA ", validators=[Length(min=1, max=5, message="min 1000 max 9999"),
                                                                   Regexp(numero_appareil_regexp,
                                                                          message="Pas de ")
                                                                   ])
    ville_appareil_wtf = StringField("Clavioter la Ville ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                       Regexp(nom_appareil_regexp,
                                                                              message="Pas de ")
                                                                       ])
    rue_appareil_wtf = StringField("Clavioter la Rue ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                   Regexp(rue_appareil_regexp,
                                                                          message="Pas de ")
                                                                   ])
    n_appareil_wtf = StringField("Clavioter le Numéro ", validators=[Length(min=1, max=5, message="min 1 max 300"),
                                                                    Regexp(n_appareil_regexp,
                                                                           message="Pas de ")
                                                                    ])
    submit = SubmitField("Enregistrer l'appareil")


class FormWTFUpdateAppareil(FlaskForm):
    """
        Dans le formulaire "appareil_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_appareil_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\-]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    rue_appareil_regexp = "^[^0-9()]+$"
    numero_appareil_regexp = "[0-9]"
    n_appareil_regexp = "[0-9_.-]"

    npa_appareil_update_wtf = StringField("Clavioter le NPA ", validators=[Length(min=1, max=5, message="min 1000 max 9999"),
                                                                   Regexp(numero_appareil_regexp,
                                                                          message="Pas de ")
                                                                   ])
    ville_appareil_update_wtf = StringField("Clavioter la Ville ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                       Regexp(nom_appareil_regexp,
                                                                              message="Pas de ")
                                                                       ])
    rue_appareil_update_wtf = StringField("Clavioter la Rue ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                   Regexp(rue_appareil_regexp,
                                                                          message="Pas de ")
                                                                   ])
    n_appareil_update_wtf = StringField("Clavioter le Numéro ", validators=[Length(min=1, max=5, message="min 1 max 300"),
                                                                    Regexp(n_appareil_regexp,
                                                                           message="Pas de ")
                                                                    ])
    submit = SubmitField("Modifier l'appareil")


class FormWTFDeleteAppareil(FlaskForm):
    """
        Dans le formulaire "appareil_delete_wtf.html"

        nom_appareil_delete_wtf : Champ qui reçoit la valeur du appareil, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "appareil".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_appareil".
    """
    npa_appareil_delete_wtf = StringField("NPA:")
    ville_appareil_delete_wtf = StringField("Ville:")
    rue_appareil_delete_wtf = StringField("Rue:")
    n_appareil_delete_wtf = StringField("N°:")
    submit_btn_del = SubmitField("Effacer cette personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
