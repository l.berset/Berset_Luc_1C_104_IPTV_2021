"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "avoir_mails_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_avoir_mail_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_avoir_mail_wtf = StringField("Clavioter le Nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_avoir_mail_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    prenom_avoir_mail_wtf = StringField("Clavioter le Prénom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                 Regexp(nom_avoir_mail_regexp,
                                                                        message="Pas de chiffres, de caractères "
                                                                                "spéciaux, "
                                                                                "d'espace à double, de double "
                                                                                "apostrophe, de double trait union")
                                                                 ])
    submit = SubmitField("Enregistrer la personne")


class FormWTFUpdateavoir_mails(FlaskForm):
    """
        Dans le formulaire "avoir_mail_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_avoir_mail_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_avoir_mail_update_wtf = StringField("Clavioter le Nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(nom_avoir_mail_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    prenom_avoir_mail_update_wtf = StringField("Clavioter le Prenom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                        Regexp(nom_avoir_mail_update_regexp,
                                                                               message="Pas de chiffres, de "
                                                                                       "caractères "
                                                                                       "spéciaux, "
                                                                                       "d'espace à double, de double "
                                                                                       "apostrophe, de double trait "
                                                                                       "union")
                                                                        ])

    submit = SubmitField("Update Personne")


class FormWTFDeleteGenres(FlaskForm):
    """
        Dans le formulaire "avoir_mail_delete_wtf.html"

        nom_avoir_mail_delete_wtf : Champ qui reçoit la valeur du avoir_mail, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "avoir_mail".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_avoir_mail".
    """
    nom_avoir_mail_delete_wtf = StringField("Nom:")
    prenom_avoir_mail_delete_wtf = StringField("Prenom:")
    submit_btn_del = SubmitField("Effacer cette personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
