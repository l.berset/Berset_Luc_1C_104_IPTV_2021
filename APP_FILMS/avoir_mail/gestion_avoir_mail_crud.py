"""
    Fichier : gestion_avoir_mail_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les genres.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *


"""
    Nom : avoir_mail_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /avoir_mail_afficher
    
    But : Afficher les films avec les genres associés pour chaque film.
    
    Paramètres : Id_Mail_sel = 0 >> tous les films.
                 Id_Mail_sel = "n" affiche le film dont l'id est "n"
                 
"""


@obj_mon_application.route("/avoir_mail_afficher/<int:Id_Personne_sel>", methods=['GET', 'POST'])
def avoir_mail_afficher(Id_Personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_avoir_mail_afficher:
                code, msg = Exception_init_avoir_mail_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_avoir_mail_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_avoir_mail_afficher.args[0]} , "
                      f"{Exception_init_avoir_mail_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_avoir_mail_afficher_data = """SELECT Id_Personne, Nom_Personne, Prenom_Personne, Naissance_Personne, GROUP_CONCAT(" ",Nom_Mail) as Persmail FROM t_avoir_mail 
                                                            RIGHT JOIN t_personne ON t_personne.Id_Personne = t_avoir_mail.FK_personne 
                                                            LEFT JOIN t_mail ON t_mail.Id_Mail = t_avoir_mail.FK_mail 
                                                            GROUP BY Id_Personne"""
                if Id_Personne_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_avoir_mail_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_Id_Personne_selected_dictionnaire = {"value_Id_Personne_selected": Id_Personne_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_avoir_mail_afficher_data += """ HAVING Id_Personne= %(value_Id_Personne_selected)s"""

                    mc_afficher.execute(strsql_avoir_mail_afficher_data, valeur_id_personne_selected_dictionnaire)

                # Récupère les données de la requête.
                data_avoir_mail_afficher = mc_afficher.fetchall()
                print("data_cetegorie ", data_avoir_mail_afficher, " Type : ",
                      type(data_avoir_mail_afficher))

                # Différencier les messages.
                if not data_avoir_mail_afficher and id_personne_sel == 0:
                    flash("""La table "t_personne" est vide. !""", "warning")
                elif not data_avoir_mail_afficher and id_personne_sel > 0:
                    # Si l'utilisateur change l'Id_Personne dans l'URL et qu'il ne correspond à aucun film
                    flash(f"Le personne {id_personne_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données personne et mail affichés !!", "success")

        except Exception as Exception_avoir_mail_afficher:
            code, msg = Exception_avoir_mail_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception avoir_mail_afficher : {sys.exc_info()[0]} "
                  f"{Exception_avoir_mail_afficher.args[0]} , "
                  f"{Exception_avoir_mail_afficher}", "danger")

    # Envoie la page "HTML" au serveur.Chris change pas sa pour l'instant
    return render_template("avoir_mail/avoir_mail_afficher.html", data=data_avoir_mail_afficher)


"""
    nom: edit_avoir_mail_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "avoir_mail_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les genres contenus dans la "t_mail".
    2) Les genres attribués au film selectionné.
    3) Les genres non-attribués au film sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_avoir_mail_selected", methods=['GET', 'POST'])
def edit_avoir_mail_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_mail_afficher = """SELECT Id_Mail, Nom_Mail FROM t_mail ORDER BY Id_Mail ASC"""
                mc_afficher.execute(strsql_mail_afficher)
            data_mail_all = mc_afficher.fetchall()
            print("dans edit_avoir_mail_selected ---> data_mail_all", data_mail_all)

            # Récupère la valeur de "Id_Personne" du formulaire html "avoir_mail_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "Id_Personne"
            # grâce à la variable "id_avoir_mail_edit_html" dans le fichier "avoir_mail_afficher.html"
            # href="{{ url_for('edit_avoir_mail_selected', id_avoir_mail_edit_html=row.Id_Personne) }}"
            id_avoir_mail_edit = request.values['id_avoir_mail_edit_html']

            # Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_avoir_mail_edit'] = id_avoir_mail_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_avoir_mail_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction avoir_mail_afficher_data
            # 1) Sélection du film choisi
            # 2) Sélection des genres "déjà" attribués pour le film.
            # 3) Sélection des genres "pas encore" attribués pour le film choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "avoir_mail_afficher_data"
            data_avoir_mail_selected, data_avoir_mail_non_attribues, data_avoir_mail_attribues = \
                avoir_mail_afficher_data(valeur_id_personne_selected_dictionnaire)

            print(data_avoir_mail_selected)
            lst_data_personne_selected = [item['Id_Personne'] for item in data_avoir_mail_selected]
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui ne sont pas encore sélectionnés.
            lst_data_avoir_mail_non_attribues = [item['Id_Mail'] for item in
                                                        data_avoir_mail_non_attribues]
            session['session_lst_data_avoir_mail_non_attribues'] = lst_data_avoir_mail_non_attribues
            print("lst_data_avoir_mail_non_attribues  ", lst_data_avoir_mail_non_attribues,
                  type(lst_data_avoir_mail_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui sont déjà sélectionnés.
            lst_data_avoir_mail_old_attribues = [item['Id_Mail'] for item in
                                                        data_avoir_mail_attribues]
            session['session_lst_data_avoir_mail_old_attribues'] = lst_data_avoir_mail_old_attribues
            print("lst_data_avoir_mail_old_attribues  ", lst_data_avoir_mail_old_attribues,
                  type(lst_data_avoir_mail_old_attribues))

            print(" data data_avoir_mail_selected", data_avoir_mail_selected, "type ",
                  type(data_avoir_mail_selected))
            print(" data data_avoir_mail_non_attribues ", data_avoir_mail_non_attribues, "type ",
                  type(data_avoir_mail_non_attribues))
            print(" data_avoir_mail_attribues ", data_avoir_mail_attribues, "type ",
                  type(data_avoir_mail_attribues))

            # Extrait les valeurs contenues dans la table "t_mails", colonne "cat_nom"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'Id_Mail
            lst_data_avoir_mail_non_attribues = [item['cat_nom'] for item in
                                                        data_avoir_mail_non_attribues]
            print("lst_all_genres gf_edit_avoir_mail_selected ", lst_data_avoir_mail_non_attribues,
                  type(lst_data_avoir_mail_non_attribues))

        except Exception as Exception_edit_avoir_mail_selected:
            code, msg = Exception_edit_avoir_mail_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_avoir_mail_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_avoir_mail_selected.args[0]} , "
                  f"{Exception_edit_avoir_mail_selected}", "danger")

    return render_template("avoir_mail/avoir_mail_modifier_tags_dropbox.html",
                           data_mail=data_mail_all,
                           data_personne_selected=data_avoir_mail_selected,
                           data_mail_attribues=data_avoir_mail_attribues,
                           data_mail_non_attribues=data_avoir_mail_non_attribues)


"""
    nom: update_avoir_mail_selected

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "avoir_mail_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les genres contenus dans la "t_mail".
    2) Les genres attribués au film selectionné.
    3) Les genres non-attribués au film sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_avoir_mail_selected", methods=['GET', 'POST'])
def update_avoir_mail_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            id_personne_selected = session['session_id_avoir_mail_edit']
            print("session['session_id_avoir_mail_edit'] ", session['session_id_avoir_mail_edit'])

            # Récupère la liste des genres qui ne sont pas associés au film sélectionné.
            old_lst_data_avoir_mail_non_attribues = session['session_lst_data_avoir_mail_non_attribues']
            print("old_lst_data_avoir_mail_non_attribues ", old_lst_data_avoir_mail_non_attribues)

            # Récupère la liste des genres qui sont associés au film sélectionné.
            old_lst_data_avoir_mail_attribues = session['session_lst_data_avoir_mail_old_attribues']
            print("old_lst_data_avoir_mail_old_attribues ", old_lst_data_avoir_mail_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme genres dans le composant "tags-selector-tagselect"
            # dans le fichier "genres_films_modifier_tags_dropbox.html"
            new_lst_str_avoir_mail = request.form.getlist('name_select_tags')
            print("new_lst_str_avoir_mail ", new_lst_str_avoir_mail)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_avoir_mail_old = list(map(int, new_lst_str_avoir_mail))
            print("new_lst_avoir_mail ", new_lst_int_avoir_mail_old, "type new_lst_avoir_mail ",
                  type(new_lst_int_avoir_mail_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "Id_Mail" qui doivent être effacés de la table intermédiaire "t_avoir_mail".
            lst_diff_mail_delete_b = list(
                set(old_lst_data_avoir_mail_attribues) - set(new_lst_int_avoir_mail_old))
            print("lst_diff_mail_delete_b ", lst_diff_mail_delete_b)

            # Une liste de "Id_Mail" qui doivent être ajoutés à la "t_avoir_mail"
            lst_diff_mail_insert_a = list(
                set(new_lst_int_avoir_mail_old) - set(old_lst_data_avoir_mail_attribues))
            print("lst_diff_mail_insert_a ", lst_diff_mail_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "FK_personne"/"Id_Personne" et "FK_mail"/"Id_Mail" dans la "t_avoir_mail"
            strsql_insert_avoir_mail = """INSERT INTO t_avoir_mail (id_avoir_mail, FK_mail, FK_personne)
                                                    VALUES (NULL, %(value_fk_mail)s, %(value_fk_personne)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "Id_Personne" et "Id_Mail" dans la "t_avoir_mail"
            strsql_delete_mail_film = """DELETE FROM t_avoir_mail WHERE FK_mail = %(value_fk_mail)s AND FK_personne = %(value_fk_personne)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le film sélectionné, parcourir la liste des genres à INSÉRER dans la "t_avoir_mail".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for Id_Mail_ins in lst_diff_mail_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "Id_Mail_ins" (l'id du genre dans la liste) associé à une variable.
                    valeurs_personne_sel_mail_sel_dictionnaire = {"value_fk_personne": id_personne_selected,
                                                                      "value_fk_mail": Id_Mail_ins}

                    mconn_bd.mabd_execute(strsql_insert_avoir_mail, valeurs_personne_sel_mail_sel_dictionnaire)

                # Pour le film sélectionné, parcourir la liste des genres à EFFACER dans la "t_avoir_mail".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for Id_Mail_del in lst_diff_mail_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "Id_Mail_del" (l'id du genre dans la liste) associé à une variable.
                    valeurs_personne_sel_mail_sel_dictionnaire = {"value_fk_personne": id_personne_selected,
                                                                      "value_fk_mail": Id_Mail_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_mail_film, valeurs_personne_sel_mail_sel_dictionnaire)

        except Exception as Exception_update_avoir_mail_selected:
            code, msg = Exception_update_avoir_mail_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_avoir_mail_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_avoir_mail_selected.args[0]} , "
                  f"{Exception_update_avoir_mail_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_avoir_mail",
    # on affiche les films et le(urs) genre(s) associé(s).
    return redirect(url_for('avoir_mail_afficher', Id_Personne_sel=Id_Personne_selected))


"""
    nom: avoir_mail_afficher_data

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "avoir_mail_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des genres, ainsi l'utilisateur voit les genres à disposition

    On signale les erreurs importantes
"""


def avoir_mail_afficher_data(valeur_id_personne_selected_dict):
    print("valeur_id_personne_selected_dict...", valeur_id_personne_selected_dict)
    try:

        strsql_personne_selected = """SELECT Id_Personne, rec_nom, rec_duree, rec_nombrePers, re_préparation, GROUP_CONCAT(Id_Mail) as Catpersonne FROM t_avoir_mail
                                        INNER JOIN t_personne ON t_personne.Id_Personne = t_avoir_mail.FK_personne
                                        INNER JOIN t_mail ON t_mail.Id_Mail = t_avoir_mail.FK_mail
                                        WHERE Id_Personne = %(value_id_personne_selected)s"""

        strsql_avoir_mail_non_attribues = """SELECT Id_Mail, cat_nom FROM t_mail WHERE Id_Mail not in(SELECT Id_Mail as idCatepersonne FROM t_avoir_mail
                                                    INNER JOIN t_personne ON t_personne.Id_Personne = t_avoir_mail.FK_personne
                                                    INNER JOIN t_mail ON t_mail.Id_Mail = t_avoir_mail.FK_mail
                                                    WHERE Id_Personne = %(value_id_personne_selected)s)"""

        strsql_avoir_mail_attribues = """SELECT Id_Personne, Id_Mail, cat_nom FROM t_avoir_mail
                                            INNER JOIN t_personne ON t_personne.Id_Personne = t_avoir_mail.FK_personne
                                            INNER JOIN t_mail ON t_mail.Id_Mail = t_avoir_mail.FK_mail
                                            WHERE Id_Personne = %(value_id_personne_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_avoir_mail_non_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_avoir_mail_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("avoir_mail_afficher_data ----> data_avoir_mail_non_attribues ",
                  data_avoir_mail_non_attribues,
                  " Type : ",
                  type(data_avoir_mail_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_personne_selected, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_personne_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_personne_selected  ", data_personne_selected, " Type : ", type(data_personne_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_avoir_mail_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_avoir_mail_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_avoir_mail_attribues ", data_avoir_mail_attribues, " Type : ",
                  type(data_avoir_mail_attribues))

            # Retourne les données des "SELECT"
            return data_personne_selected, data_avoir_mail_non_attribues, data_avoir_mail_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans avoir_mail_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans avoir_mail_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_avoir_mail_afficher_data:
        code, msg = IntegrityError_avoir_mail_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans avoir_mail_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_avoir_mail_afficher_data.args[0]} , "
              f"{IntegrityError_avoir_mail_afficher_data}", "danger")


