"""
    Fichier : gestion_adresses_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les adresses.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.Adresse.gestion_adresse_wtf_forms import FormWTFAjouterAdresse
from APP_FILMS.Adresse.gestion_adresse_wtf_forms import FormWTFDeleteAdresse
from APP_FILMS.Adresse.gestion_adresse_wtf_forms import FormWTFUpdateAdresse

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /adresses_afficher
    
    Test : ex : http://127.0.0.1:5005/adresses_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_adresse_sel = 0 >> tous les adresses.
                id_adresse_sel = "n" affiche le adresse dont l'id est "n"
"""


@obj_mon_application.route("/adresse_afficher/<string:order_by>/<int:Id_Personne_sel>", methods=['GET', 'POST'])
def adresse_afficher(order_by, Id_Personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion adresse ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionAdresses {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and Id_Personne_sel == 0:
                    strsql_adresse_afficher = """SELECT Id_Adresse, NPA, Ville, Rue, N FROM t_adresse ORDER BY Id_Adresse ASC"""
                    mc_afficher.execute(strsql_adresse_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_adresse"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du adresse sélectionné avec un nom de variable
                    valeur_id_adresse_selected_dictionnaire = {"value_id_adresse_selected": Id_Personne_sel}
                    strsql_adresse_afficher = """SELECT Id_Adresse, NPA, Ville, Rue, N FROM t_adresse  WHERE Id_Adresse = %(value_id_adresse_selected)s"""

                    mc_afficher.execute(strsql_adresse_afficher, valeur_id_adresse_selected_dictionnaire)
                else:
                    strsql_adresse_afficher = """SELECT Id_Adresse, NPA, Ville, Rue, N FROM t_adresse ORDER BY Id_Adresse DESC"""

                    mc_afficher.execute(strsql_adresse_afficher)

                data_personne = mc_afficher.fetchall()

                print("data_personne ", data_personne, " Type : ", type(data_personne))

                # Différencier les messages si la table est vide.
                if not data_personne and Id_Personne_sel == 0:
                    flash("""La table "t_adresse" est vide. !!""", "warning")
                elif not data_personne and Id_Personne_sel > 0:
                    # Si l'utilisateur change l'id_adresse dans l'URL et que le adresse n'existe pas,
                    flash(f"La personne_sel demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_adresse" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données adresses affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. adresse_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} adresse_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("adresse/adresse_afficher.html", data=data_personne)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /adresses_ajouter
    
    Test : ex : http://127.0.0.1:5005/adresses_ajouter
    
    Paramètres : sans
    
    But : Ajouter un adresse pour un film
    
    Remarque :  Dans le champ "name_adresse_html" du formulaire "adresses/adresses_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/adresse_ajouter", methods=['GET', 'POST'])
def adresse_ajouter_wtf():
    form = FormWTFAjouterAdresse()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion adresses ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionadresses {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                npa_adresse_wtf = form.npa_adresse_wtf.data
                npa_adresse = npa_adresse_wtf.lower()
                ville_wtf = form.ville_adresse_wtf.data
                ville = ville_wtf.capitalize()
                rue_wtf = form.rue_adresse_wtf.data
                rue = rue_wtf.capitalize()
                n_wtf = form.n_adresse_wtf.data
                n = n_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_npa": npa_adresse,"value_ville":ville,"value_rue":rue,"value_n":n}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_Nom_Personne = """INSERT INTO t_adresse (Id_Adresse,NPA,Ville,Rue,N) VALUES (NULL,%(value_npa)s,%(value_ville)s,%(value_rue)s,%(value_n)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_Nom_Personne, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('adresse_afficher', order_by='DESC', Id_Personne_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_personne_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_personne_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion adresse CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("adresse/adresse_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /adresse_update
    
    Test : ex cliquer sur le menu "adresses" puis cliquer sur le bouton "EDIT" d'un "adresse"
    
    Paramètres : sans
    
    But : Editer(update) un adresse qui a été sélectionné dans le formulaire "adresses_afficher.html"
    
    Remarque :  Dans le champ "nom_adresse_update_wtf" du formulaire "adresses/adresse_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/adresse_update", methods=['GET', 'POST'])
def adresse_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_adresse"
    id_adresse_update = request.values['id_adresse_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateAdresse()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "adresse_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            npa_adresse_wtf = form_update.npa_adresse_update_wtf.data
            npa_adresse = npa_adresse_wtf.lower()
            ville_wtf = form_update.ville_adresse_update_wtf.data
            ville = ville_wtf.capitalize()
            rue_wtf = form_update.rue_adresse_update_wtf.data
            rue = rue_wtf.capitalize()
            n_wtf = form_update.n_adresse_update_wtf.data
            n = n_wtf.lower()

            valeur_update_dictionnaire = {"value_id_adresse": id_adresse_update, "value_npa": npa_adresse,
                                          "value_ville": ville, "value_rue": rue, "value_n": n}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intituleadresse = """UPDATE t_adresse SET NPA= %(value_npa)s,
                                                Ville= %(value_ville)s, Rue= %(value_rue)s, N= %(value_n)s WHERE Id_Adresse = %(value_id_adresse)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituleadresse, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_adresse_update"
            return redirect(url_for('adresse_afficher', order_by="ASC", Id_Personne_sel=id_adresse_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_adresse" et "intitule_adresse" de la "t_adresse"
            str_sql_id_adresse = "SELECT Id_Adresse, NPA, Ville, Rue, N FROM t_adresse WHERE Id_Adresse = %(value_id_adresse)s"
            valeur_select_dictionnaire = {"value_id_adresse": id_adresse_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_adresse, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom adresse" pour l'UPDATE
            data_nom_adresse = mybd_curseur.fetchone()
            print("data_nom_adresse ", data_nom_adresse, " type ", type(data_nom_adresse), " adresse ",
                  data_nom_adresse["NPA"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "adresse_update_wtf.html"
            form_update.npa_adresse_update_wtf.data = data_nom_adresse["NPA"]
            form_update.ville_adresse_update_wtf.data = data_nom_adresse["Ville"]
            form_update.rue_adresse_update_wtf.data = data_nom_adresse["Rue"]
            form_update.n_adresse_update_wtf.data = data_nom_adresse["N"]
    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans adresse_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans adresse_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans adresse_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans adresse_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("adresse/adresse_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /adresse_delete
    
    Test : ex. cliquer sur le menu "adresses" puis cliquer sur le bouton "DELETE" d'un "adresse"
    
    Paramètres : sans
    
    But : Effacer(delete) un adresse qui a été sélectionné dans le formulaire "adresses_afficher.html"
    
    Remarque :  Dans le champ "nom_adresse_delete_wtf" du formulaire "adresses/adresse_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/adresse_delete", methods=['GET', 'POST'])
def adresse_delete_wtf():
    data_films_attribue_adresse_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_adresse"
    id_adresse_delete = request.values['id_adresse_btn_delete_html']

    # Objet formulaire pour effacer le adresse sélectionné.
    form_delete = FormWTFDeleteAdresse()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("adresse_afficher", order_by="ASC", Id_Personne_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "adresses/adresse_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_adresse_delete = session['data_films_attribue_adresse_delete']
                print("data_films_attribue_adresse_delete ", data_films_attribue_adresse_delete)

                flash(f"Effacer le adresse de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer adresse" qui va irrémédiablement EFFACER le adresse
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_adresse": id_adresse_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_adresse = """DELETE FROM t_adresse WHERE Id_Adresse = %(value_id_adresse)s"""
                str_sql_delete_idadresse = """DELETE FROM t_adresse WHERE Id_Adresse = %(value_id_adresse)s"""
                # Manière brutale d'effacer d'abord la "fk_adresse", même si elle n'existe pas dans la "t_adresse_film"
                # Ensuite on peut effacer le adresse vu qu'il n'est plus "lié" (INNODB) dans la "t_adresse_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_adresse, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idadresse, valeur_delete_dictionnaire)

                flash(f"adresse définitivement effacé !!", "success")
                print(f"adresse définitivement effacé !!")

                # afficher les données
                return redirect(url_for('adresse_afficher', order_by="ASC", Id_Personne_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_adresse": id_adresse_delete}
            print(id_adresse_delete, type(id_adresse_delete))

            # Requête qui affiche tous les films qui ont le adresse que l'utilisateur veut effacer
            str_sql_adresses_films_delete = """SELECT Id_Adresse, NPA, Ville, Rue, N FROM t_adresse WHERE Id_Adresse= %(value_id_adresse)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_adresses_films_delete, valeur_select_dictionnaire)
            data_films_attribue_adresse_delete = mybd_curseur.fetchall()
            print("data_films_attribue_adresse_delete...", data_films_attribue_adresse_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "adresses/adresse_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_adresse_delete'] = data_films_attribue_adresse_delete

            # Opération sur la BD pour récupérer "id_adresse" et "intitule_adresse" de la "t_adresse"
            str_sql_id_adresse = "SELECT Id_Adresse, NPA, Ville, Rue, N FROM t_adresse WHERE Id_Adresse = %(value_id_adresse)s"

            mybd_curseur.execute(str_sql_id_adresse, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom adresse" pour l'action DELETE
            data_nom_adresse = mybd_curseur.fetchone()
            print("data_nom_adresse ", data_nom_adresse, " type ", type(data_nom_adresse), " NPA ",
                  data_nom_adresse["NPA"],"Ville", data_nom_adresse["Ville"], "Rue", data_nom_adresse["Rue"], "N", data_nom_adresse["N"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "adresse_delete_wtf.html"
            form_delete.npa_adresse_delete_wtf.data = data_nom_adresse["NPA"]
            form_delete.ville_adresse_delete_wtf.data = data_nom_adresse["Ville"]
            form_delete.rue_adresse_delete_wtf.data = data_nom_adresse["Rue"]
            form_delete.n_adresse_delete_wtf.data = data_nom_adresse["N"]

            # Le bouton pour l'action "DELETE" dans le form. "adresse_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans adresse_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans adresse_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans adresse_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans adresse_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("adresse/adresse_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_adresse_delete)
