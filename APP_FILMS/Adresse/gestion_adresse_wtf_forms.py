"""
    Fichier : gestion_adresses_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterAdresse(FlaskForm):
    """
        Dans le formulaire "adresses_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_adresse_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\-]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    rue_adresse_regexp = "^[^0-9()]+$"
    numero_adresse_regexp = "[0-9]"
    n_adresse_regexp = "[0-9_.-]"

    npa_adresse_wtf = StringField("Clavioter le NPA ", validators=[Length(min=1, max=5, message="min 1000 max 9999"),
                                                                   Regexp(numero_adresse_regexp,
                                                                          message="Pas de ")
                                                                   ])
    ville_adresse_wtf = StringField("Clavioter la Ville ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                       Regexp(nom_adresse_regexp,
                                                                              message="Pas de ")
                                                                       ])
    rue_adresse_wtf = StringField("Clavioter la Rue ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                   Regexp(rue_adresse_regexp,
                                                                          message="Pas de ")
                                                                   ])
    n_adresse_wtf = StringField("Clavioter le Numéro ", validators=[Length(min=1, max=5, message="min 1 max 300"),
                                                                    Regexp(n_adresse_regexp,
                                                                           message="Pas de ")
                                                                    ])
    submit = SubmitField("Enregistrer l'adresse")


class FormWTFUpdateAdresse(FlaskForm):
    """
        Dans le formulaire "adresse_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_adresse_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\-]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    rue_adresse_regexp = "^[^0-9()]+$"
    numero_adresse_regexp = "[0-9]"
    n_adresse_regexp = "[0-9_.-]"

    npa_adresse_update_wtf = StringField("Clavioter le NPA ", validators=[Length(min=1, max=5, message="min 1000 max 9999"),
                                                                   Regexp(numero_adresse_regexp,
                                                                          message="Pas de ")
                                                                   ])
    ville_adresse_update_wtf = StringField("Clavioter la Ville ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                       Regexp(nom_adresse_regexp,
                                                                              message="Pas de ")
                                                                       ])
    rue_adresse_update_wtf = StringField("Clavioter la Rue ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                   Regexp(rue_adresse_regexp,
                                                                          message="Pas de ")
                                                                   ])
    n_adresse_update_wtf = StringField("Clavioter le Numéro ", validators=[Length(min=1, max=5, message="min 1 max 300"),
                                                                    Regexp(n_adresse_regexp,
                                                                           message="Pas de ")
                                                                    ])
    submit = SubmitField("Modifier l'adresse")


class FormWTFDeleteAdresse(FlaskForm):
    """
        Dans le formulaire "adresse_delete_wtf.html"

        nom_adresse_delete_wtf : Champ qui reçoit la valeur du adresse, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "adresse".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_adresse".
    """
    npa_adresse_delete_wtf = StringField("NPA:")
    ville_adresse_delete_wtf = StringField("Ville:")
    rue_adresse_delete_wtf = StringField("Rue:")
    n_adresse_delete_wtf = StringField("N°:")
    submit_btn_del = SubmitField("Effacer cette personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
