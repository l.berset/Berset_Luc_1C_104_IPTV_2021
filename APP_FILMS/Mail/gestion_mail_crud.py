"""
    Fichier : gestion_mails_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les mail.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.Mail.gestion_mail_wtf_forms import FormWTFAjouterMail
from APP_FILMS.Mail.gestion_mail_wtf_forms import FormWTFDeleteMail
from APP_FILMS.Mail.gestion_mail_wtf_forms import FormWTFUpdateMail

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /mails_afficher
    
    Test : ex : http://127.0.0.1:5005/mails_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_mail_sel = 0 >> tous les mails.
                id_mail_sel = "n" affiche le mail dont l'id est "n"
"""


@obj_mon_application.route("/mail_afficher/<string:order_by>/<int:Id_Personne_sel>", methods=['GET', 'POST'])
def mail_afficher(order_by, Id_Personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion mail ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionMails {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and Id_Personne_sel == 0:
                    strsql_mail_afficher = """SELECT Id_Mail, Nom_Mail FROM t_mail ORDER BY Id_Mail ASC"""
                    mc_afficher.execute(strsql_mail_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_mail"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du mail sélectionné avec un nom de variable
                    valeur_id_mail_selected_dictionnaire = {"value_id_mail_selected": Id_Personne_sel}
                    strsql_mail_afficher = """SELECT Id_Mail, Nom_Mail FROM t_mail  WHERE Id_Mail = %(value_id_mail_selected)s"""

                    mc_afficher.execute(strsql_mail_afficher, valeur_id_mail_selected_dictionnaire)
                else:
                    strsql_mail_afficher = """SELECT Id_Mail, Nom_Mail FROM t_mail ORDER BY Id_Mail DESC"""

                    mc_afficher.execute(strsql_mail_afficher)

                data_personne = mc_afficher.fetchall()

                print("data_personne ", data_personne, " Type : ", type(data_personne))

                # Différencier les messages si la table est vide.
                if not data_personne and Id_Personne_sel == 0:
                    flash("""La table "t_mail" est vide. !!""", "warning")
                elif not data_personne and Id_Personne_sel > 0:
                    # Si l'utilisateur change l'id_mail dans l'URL et que le mail n'existe pas,
                    flash(f"La personne_sel demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_mail" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données mails affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. mail_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} mail_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("mail/mail_afficher.html", data=data_personne)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /mails_ajouter
    
    Test : ex : http://127.0.0.1:5005/mails_ajouter
    
    Paramètres : sans
    
    But : Ajouter un mail pour un film
    
    Remarque :  Dans le champ "name_mail_html" du formulaire "mails/mails_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/mail_ajouter", methods=['GET', 'POST'])
def mail_ajouter_wtf():
    form = FormWTFAjouterMail()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion mails ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionmails {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                nom_mail_wtf = form.nom_mail_wtf.data
                nom_mail = nom_mail_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_nom": nom_mail}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_Nom_Personne = """INSERT INTO t_mail (Id_Mail,Nom_Mail) VALUES (NULL,%(value_nom)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_Nom_Personne, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('mail_afficher', order_by='DESC', Id_Personne_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_personne_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_personne_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion mail CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("mail/mail_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /mail_update
    
    Test : ex cliquer sur le menu "mails" puis cliquer sur le bouton "EDIT" d'un "mail"
    
    Paramètres : sans
    
    But : Editer(update) un mail qui a été sélectionné dans le formulaire "mails_afficher.html"
    
    Remarque :  Dans le champ "nom_mail_update_wtf" du formulaire "mails/mail_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/mail_update", methods=['GET', 'POST'])
def mail_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_mail"
    id_mail_update = request.values['id_mail_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateMail()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "mail_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            nom_mail_wtf = form_update.nom_mail_update_wtf.data
            nom_mail = nom_mail_wtf.lower()


            valeur_update_dictionnaire = {"value_id_mail": id_mail_update, "value_nom": nom_mail}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulemail = """UPDATE t_mail SET Nom_Mail= %(value_nom)s WHERE Id_Mail = %(value_id_mail)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulemail, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_mail_update"
            return redirect(url_for('mail_afficher', order_by="ASC", Id_Personne_sel=id_mail_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_mail" et "intitule_mail" de la "t_mail"
            str_sql_id_mail = "SELECT Id_Mail, Nom_Mail FROM t_mail WHERE Id_Mail = %(value_id_mail)s"
            valeur_select_dictionnaire = {"value_id_mail": id_mail_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_mail, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom mail" pour l'UPDATE
            data_nom_mail = mybd_curseur.fetchone()
            print("data_nom_mail ", data_nom_mail, " type ", type(data_nom_mail), " mail ",
                  data_nom_mail["Nom_Mail"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "mail_update_wtf.html"
            form_update.nom_mail_update_wtf.data = data_nom_mail["Nom_Mail"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans mail_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans mail_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans mail_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans mail_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("mail/mail_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /mail_delete
    
    Test : ex. cliquer sur le menu "mails" puis cliquer sur le bouton "DELETE" d'un "mail"
    
    Paramètres : sans
    
    But : Effacer(delete) un mail qui a été sélectionné dans le formulaire "mails_afficher.html"
    
    Remarque :  Dans le champ "nom_mail_delete_wtf" du formulaire "mails/mail_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/mail_delete", methods=['GET', 'POST'])
def mail_delete_wtf():
    data_films_attribue_mail_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_mail"
    id_mail_delete = request.values['id_mail_btn_delete_html']

    # Objet formulaire pour effacer le mail sélectionné.
    form_delete = FormWTFDeleteMail()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("mail_afficher", order_by="ASC", Id_Personne_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "mails/mail_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_mail_delete = session['data_films_attribue_mail_delete']
                print("data_films_attribue_mail_delete ", data_films_attribue_mail_delete)

                flash(f"Effacer le mail de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer mail" qui va irrémédiablement EFFACER le mail
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_mail": id_mail_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_mail = """DELETE FROM t_mail WHERE Id_Mail = %(value_id_mail)s"""
                str_sql_delete_idmail = """DELETE FROM t_mail WHERE Id_Mail = %(value_id_mail)s"""
                # Manière brutale d'effacer d'abord la "fk_mail", même si elle n'existe pas dans la "t_mail_film"
                # Ensuite on peut effacer le mail vu qu'il n'est plus "lié" (INNODB) dans la "t_mail_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_mail, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idmail, valeur_delete_dictionnaire)

                flash(f"mail définitivement effacé !!", "success")
                print(f"mail définitivement effacé !!")

                # afficher les données
                return redirect(url_for('mail_afficher', order_by="ASC", Id_Personne_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_mail": id_mail_delete}
            print(id_mail_delete, type(id_mail_delete))

            # Requête qui affiche tous les films qui ont le mail que l'utilisateur veut effacer
            str_sql_mails_films_delete = """SELECT Id_Mail, Nom_Mail FROM t_mail WHERE Id_Mail= %(value_id_mail)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_mails_films_delete, valeur_select_dictionnaire)
            data_films_attribue_mail_delete = mybd_curseur.fetchall()
            print("data_films_attribue_mail_delete...", data_films_attribue_mail_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "mails/mail_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_mail_delete'] = data_films_attribue_mail_delete

            # Opération sur la BD pour récupérer "id_mail" et "intitule_mail" de la "t_mail"
            str_sql_id_mail = "SELECT Id_Mail, Nom_Mail FROM t_mail WHERE Id_Mail = %(value_id_mail)s"

            mybd_curseur.execute(str_sql_id_mail, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom mail" pour l'action DELETE
            data_nom_mail = mybd_curseur.fetchone()
            print("data_nom_mail ", data_nom_mail, " type ", type(data_nom_mail), " Nom_Mail ",
                  data_nom_mail["Nom_Mail"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "mail_delete_wtf.html"
            form_delete.nom_mail_delete_wtf.data = data_nom_mail["Nom_Mail"]


            # Le bouton pour l'action "DELETE" dans le form. "mail_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans mail_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans mail_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans mail_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans mail_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("mail/mail_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_mail_delete)
