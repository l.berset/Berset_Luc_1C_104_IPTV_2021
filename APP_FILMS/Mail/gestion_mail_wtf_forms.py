"""
    Fichier : gestion_mails_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterMail(FlaskForm):
    """
        Dans le formulaire "mails_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_mail_regexp = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"


    nom_mail_wtf = StringField("Clavioter le Mail ", validators=[Length(min=1, max=32, message="min 1000 max 9999"),
                                                                   Regexp(nom_mail_regexp,
                                                                          message="Pas de ")
                                                                   ])

    submit = SubmitField("Enregistrer le mail")


class FormWTFUpdateMail(FlaskForm):
    """
        Dans le formulaire "mail_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_mail_regexp = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"


    nom_mail_update_wtf = StringField("Clavioter le Mail ", validators=[Length(min=1, max=50, message="min 1000 max 9999"),
                                                                   Regexp(nom_mail_regexp,
                                                                          message="Pas de ")
                                                                   ])

    submit = SubmitField("Modifier le mail")


class FormWTFDeleteMail(FlaskForm):
    """
        Dans le formulaire "mail_delete_wtf.html"

        nom_mail_delete_wtf : Champ qui reçoit la valeur du mail, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "mail".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_mail".
    """
    nom_mail_delete_wtf = StringField("Nom_Mail:")

    submit_btn_del = SubmitField("Effacer cette personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
